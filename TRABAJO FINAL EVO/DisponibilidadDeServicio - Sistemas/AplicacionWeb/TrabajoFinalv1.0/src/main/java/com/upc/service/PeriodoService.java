package com.upc.service;

import com.upc.entity.Periodo;

public interface PeriodoService {
	Iterable<Periodo> listAllPeriodo();
	Periodo getPeriodoById(Integer id);
	Periodo savePeriodo(Periodo periodo);
	void deletePerido(Integer id);
}
