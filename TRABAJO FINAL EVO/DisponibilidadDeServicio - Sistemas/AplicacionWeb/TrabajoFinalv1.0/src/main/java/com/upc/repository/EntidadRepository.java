package com.upc.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.upc.entity.Entidad;

@Repository
@Transactional
public interface EntidadRepository extends CrudRepository<Entidad, Integer> {
	List<Entidad> findByPaisContaining(String pais);
}
