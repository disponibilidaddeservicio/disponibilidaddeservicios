package com.upc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.upc.entity.Entidad;
import com.upc.repository.EntidadRepository;

@Service
public class EntidadServiceImpl implements EntidadService{

	@Autowired
	private EntidadRepository entidadRepository;
	@Override
	public Iterable<Entidad> listAllEntidad() {
		// TODO Auto-generated method stub
		return entidadRepository.findAll();
	}

	@Override
	public Entidad getEntidadById(Integer id) {
		// TODO Auto-generated method stub
		return entidadRepository.findOne(id);
	}

	@Override
	public Entidad saveEntidad(Entidad entidad) {
		// TODO Auto-generated method stub
		return entidadRepository.save(entidad);
	}

	@Override
	public void deleteEntidad(Integer id) {
		// TODO Auto-generated method stub
		entidadRepository.delete(id);
	}
	
	@Override
	public Iterable<Entidad> getEntidadByPais(String pais){
		return entidadRepository.findByPaisContaining(pais);
	}

}
