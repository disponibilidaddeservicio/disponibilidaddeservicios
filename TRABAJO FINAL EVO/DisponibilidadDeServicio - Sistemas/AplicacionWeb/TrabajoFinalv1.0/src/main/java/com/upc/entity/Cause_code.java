package com.upc.entity;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Cause_code {
	
	@Id
	@GeneratedValue(strategy=javax.persistence.GenerationType.AUTO)
	private int id_cause_code;
	private String nombre;
	private String descripcion;
	
	@OneToMany(mappedBy = "cause_code")
	private Set<Incidente> incidentes;
	
	public Cause_code() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getId_cause_code() {
		return id_cause_code;
	}
	public void setId_cause_code(int id_cause_code) {
		this.id_cause_code = id_cause_code;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public Set<Incidente> getIncidentes() {
		return incidentes;
	}
	public void setIncidentes(Set<Incidente> incidentes) {
		this.incidentes = incidentes;
	}
}
