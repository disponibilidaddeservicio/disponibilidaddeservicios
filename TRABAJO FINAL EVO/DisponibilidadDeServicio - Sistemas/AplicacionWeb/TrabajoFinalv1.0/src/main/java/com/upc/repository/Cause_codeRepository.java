package com.upc.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.upc.entity.Cause_code;

@Repository
@Transactional
public interface Cause_codeRepository extends CrudRepository<Cause_code, Integer>{

}
