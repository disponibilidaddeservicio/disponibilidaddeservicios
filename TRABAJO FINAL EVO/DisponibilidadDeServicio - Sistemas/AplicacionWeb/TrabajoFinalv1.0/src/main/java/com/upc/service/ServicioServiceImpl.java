package com.upc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.upc.entity.Servicio;
import com.upc.repository.ServicioRepository;

@Service
public class ServicioServiceImpl implements ServicioService{

	@Autowired
	private ServicioRepository servicioRepository;

	@Override
	public Iterable<Servicio> ListAllServicios() {
		// TODO Auto-generated method stub
		return servicioRepository.findAll();
	}

	@Override
	public Servicio getServicioById(Integer id) {
		// TODO Auto-generated method stub
		return servicioRepository.findOne(id);
	}

	@Override
	public Servicio saveServicio(Servicio tipoServicio) {
		// TODO Auto-generated method stub
		return servicioRepository.save(tipoServicio);
	}

	@Override
	public void deleteServicio(Integer id) {
		// TODO Auto-generated method stub
		servicioRepository.delete(id);
	}

}
