package com.upc.entity;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Periodo")
public class Periodo {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id_periodo;
	private String nombre;
	private int semanas;
	private String fecha_ini;
	private String fecha_fin;
	private int quarter;
	private int fiscal_year;
	private String request_date;
	private String delivery_date;
	
	@OneToMany(mappedBy = "periodo")
	private Set<Incidente> incidentes;
	
	public Periodo() {
		super();
	}

	public int getId_periodo() {
		return id_periodo;
	}

	public void setId_periodo(int id_periodo) {
		this.id_periodo = id_periodo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getSemanas() {
		return semanas;
	}

	public void setSemanas(int semanas) {
		this.semanas = semanas;
	}

	public String getFecha_ini() {
		return fecha_ini;
	}

	public void setFecha_ini(String fecha_ini) {
		this.fecha_ini = fecha_ini;
	}

	public String getFecha_fin() {
		return fecha_fin;
	}

	public void setFecha_fin(String fecha_fin) {
		this.fecha_fin = fecha_fin;
	}

	public int getQuarter() {
		return quarter;
	}

	public void setQuarter(int quarter) {
		this.quarter = quarter;
	}

	public int getFiscal_year() {
		return fiscal_year;
	}

	public void setFiscal_year(int fiscal_year) {
		this.fiscal_year = fiscal_year;
	}

	public String getRequest_date() {
		return request_date;
	}

	public void setRequest_date(String request_date) {
		this.request_date = request_date;
	}

	public String getDelivery_date() {
		return delivery_date;
	}

	public void setDelivery_date(String delivery_date) {
		this.delivery_date = delivery_date;
	}

	public Set<Incidente> getIncidentes() {
		return incidentes;
	}

	public void setIncidentes(Set<Incidente> incidentes) {
		this.incidentes = incidentes;
	}
}
