package com.upc.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Derivacion {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id_derivacion;
	private String area;
	private String inicio;
	private String fin;
	private String descripcion;
	
	@ManyToOne
	@JoinColumn(name="incidente")
	private Incidente incidente;

	public int getIdderivacion() {
		return id_derivacion;
	}

	public void setIdderivacion(int idderivacion) {
		this.id_derivacion = idderivacion;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getInicio() {
		return inicio;
	}

	public void setInicio(String inicio) {
		this.inicio = inicio;
	}

	public String getFin() {
		return fin;
	}

	public void setFin(String fin) {
		this.fin = fin;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Incidente getIncidente() {
		return incidente;
	}

	public void setIncidente(Incidente incidente) {
		this.incidente = incidente;
	}
	
	

}
