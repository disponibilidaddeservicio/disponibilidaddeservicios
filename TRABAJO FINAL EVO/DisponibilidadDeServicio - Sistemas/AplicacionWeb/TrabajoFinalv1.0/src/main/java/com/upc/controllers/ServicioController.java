package com.upc.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.upc.entity.Servicio;
import com.upc.service.ServicioService;

@Controller
public class ServicioController {
	
	@Autowired
	private ServicioService servicioService;
	
	@RequestMapping(value = "/servicios", method = RequestMethod.GET)
	public String list(Model model){
		model.addAttribute("servicios", servicioService.ListAllServicios());
		return "servicios";
	}
	
	@RequestMapping("/servicio/new")
	public String newServicio(Model model){
		model.addAttribute("servicio", new Servicio());
		return "servicioform";
	}
	
	@RequestMapping(value = "/servicio", method = RequestMethod.POST)
	public String saveServicio(Servicio servicio){
		servicioService.saveServicio(servicio);
		return "redirect:/servicios";
	}
	@RequestMapping("servicio/{id}")
	public String showServicio(@PathVariable Integer id, Model model){
		model.addAttribute("servicio", servicioService.getServicioById(id));
		return "servicioshow";
	}
	@RequestMapping("servicio/edit/{id}")
	public String editServicio(@PathVariable Integer id, Model model){
		model.addAttribute("servicio", servicioService.getServicioById(id));
		return "servicioform";
	}
	@RequestMapping("servicio/delete/{id}")
	public String deleteService(@PathVariable Integer id){
		servicioService.deleteServicio(id);
		return "redirect:/servicios";
	}
}
