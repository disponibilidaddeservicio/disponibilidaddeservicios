package com.upc.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Impact_color {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id_impact_color;
	private String nombre;
	private String descripcion;
	
	public Impact_color() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getId_impact_color() {
		return id_impact_color;
	}
	public void setId_impact_color(int id_impact_color) {
		this.id_impact_color = id_impact_color;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
