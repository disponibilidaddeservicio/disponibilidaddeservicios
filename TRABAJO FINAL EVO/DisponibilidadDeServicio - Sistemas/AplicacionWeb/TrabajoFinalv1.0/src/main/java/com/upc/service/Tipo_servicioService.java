package com.upc.service;

import com.upc.entity.Tipo_servicio;

public interface Tipo_servicioService {
	
	Iterable<Tipo_servicio> ListAllTipoServicios();
	Tipo_servicio getTipoServicioId(Integer id);
	Tipo_servicio saveTipoServicio(Tipo_servicio tipo_servicio);
	void deleteTipoServicio(Integer id);
}
