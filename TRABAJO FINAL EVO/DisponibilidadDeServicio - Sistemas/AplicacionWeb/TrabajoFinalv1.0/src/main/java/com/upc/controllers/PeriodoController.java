package com.upc.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.upc.entity.Periodo;
import com.upc.service.PeriodoService;

@Controller
public class PeriodoController {
	
	@Autowired
	private PeriodoService periodoService;
	
	@RequestMapping(value = "/periodos", method = RequestMethod.GET)
	public String list(Model model) {
		model.addAttribute("periodos", periodoService.listAllPeriodo());
		return "periodos";
	}
	
	@RequestMapping("periodo/new")
	public String newPeriodo(Model model) {
		model.addAttribute("periodo", new Periodo());
		return "periodos";
	}

	@RequestMapping(value = "periodo", method = RequestMethod.POST)
	public String savePeriodo(Periodo periodo) {
		periodoService.savePeriodo(periodo);
		return "redirect:/periodos";
	}

	@RequestMapping("periodo/{id}")
	public String viewPeriodo(@PathVariable Integer id, Model model) {
		model.addAttribute("periodo", periodoService.getPeriodoById(id));
		return "viewPeriodo";
	}

	@RequestMapping("periodo/edit/{id}")
	public String editPeriodo(@PathVariable Integer id, Model model) {
		model.addAttribute("periodo", periodoService.getPeriodoById(id));
		return "editPeriodo";
	}

	@RequestMapping("periodo/delete/{id}")
	public String delete(@PathVariable Integer id) {
		periodoService.deletePerido(id);
		return "redirect:/periodos";
	}

}
