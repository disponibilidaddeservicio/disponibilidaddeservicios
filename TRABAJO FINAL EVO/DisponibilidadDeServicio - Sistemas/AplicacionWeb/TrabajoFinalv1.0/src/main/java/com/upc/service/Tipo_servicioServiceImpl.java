package com.upc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.upc.entity.Tipo_servicio;
import com.upc.repository.Tipo_servicioRepository;

@Service
public class Tipo_servicioServiceImpl implements Tipo_servicioService{

	@Autowired
	private Tipo_servicioRepository tipoServicioRepository;
	
	@Override
	public Iterable<Tipo_servicio> ListAllTipoServicios() {
		// TODO Auto-generated method stub
		return tipoServicioRepository.findAll();
	}

	@Override
	public Tipo_servicio getTipoServicioId(Integer id) {
		// TODO Auto-generated method stub
		return tipoServicioRepository.findOne(id);
	}

	@Override
	public Tipo_servicio saveTipoServicio(Tipo_servicio tipo_servicio) {
		// TODO Auto-generated method stub
		return tipoServicioRepository.save(tipo_servicio);
	}

	@Override
	public void deleteTipoServicio(Integer id) {
		// TODO Auto-generated method stub
		tipoServicioRepository.delete(id);
	}

}
