package com.upc.entity;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="tipo_servicio")
public class Tipo_servicio {
	private int id_tipo_servicio;
	private String nombre;
	private String descripcion;
	private Set<Servicio> servicios;
	
	public Tipo_servicio() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Id
	@GeneratedValue( strategy = GenerationType.AUTO)
	public int getId_tipo_servicio() {
		return id_tipo_servicio;
	}

	public void setId_tipo_servicio(int id_tipo_servicio) {
		this.id_tipo_servicio = id_tipo_servicio;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@OneToMany(mappedBy = "tipo_servicio")
	public Set<Servicio> getServicios() {
		return servicios;
	}
	public void setServicios(Set<Servicio> servicios) {
		this.servicios = servicios;
	}
}
