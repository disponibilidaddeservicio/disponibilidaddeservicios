package com.upc.service;

import com.upc.entity.Servicio;

public interface ServicioService {
	Iterable<Servicio> ListAllServicios();
	Servicio getServicioById(Integer id);
	Servicio saveServicio(Servicio tipoServicio);
	void deleteServicio(Integer id);
}
