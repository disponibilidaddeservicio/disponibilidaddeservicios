package com.upc.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Impact_Type")
public class Impact_type {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id_impact_type;
	
	@Column(name= "nombre")
	private String nombre;
	
	private String descripcion;
	
	@OneToMany(mappedBy="impacttype")
	private Set<Incidente> incidentes;
	
	public Impact_type() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getId_impact_type() {
		return id_impact_type;
	}
	public void setId_impact_type(int id_impact_type) {
		this.id_impact_type = id_impact_type;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public Set<Incidente> getIncidentes() {
		return incidentes;
	}
	public void setIncidentes(Set<Incidente> incidentes) {
		this.incidentes = incidentes;
	}
	
	
}
