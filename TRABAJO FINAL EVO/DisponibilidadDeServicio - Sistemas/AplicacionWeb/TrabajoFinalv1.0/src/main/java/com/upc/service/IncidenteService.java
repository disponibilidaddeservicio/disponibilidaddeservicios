package com.upc.service;

import com.upc.entity.Incidente;

public interface IncidenteService {

	Iterable<Incidente> listAllIncidente();
	Incidente getIncidenteById(Integer id);
	Incidente saveIncidente(Incidente incidente);
	void deleteIncidente(Integer id);
}
