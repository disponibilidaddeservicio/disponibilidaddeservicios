package com.upc.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.upc.entity.Entidad;
import com.upc.service.EntidadService;

@Controller
public class EntidadController {

	@Autowired
	private EntidadService entidadService;
	
	@RequestMapping(value = "/entidades", method = RequestMethod.GET)
	public String listarEntidades(Model model) {
		model.addAttribute("entidades", entidadService.listAllEntidad());
		return "entidades";
	}
	
	@RequestMapping(value = "/entidades", method = RequestMethod.POST)
	public String buscarEntidades(Model model, @RequestParam String pais) {
		model.addAttribute("entidades", entidadService.getEntidadByPais(pais));
		return "entidades";
	}
	
	@RequestMapping("/entidad/new")
	public String newEntidad(Model model) {
		model.addAttribute("entidad", new Entidad());
		return "newEntidad";
	}

	@RequestMapping(value = "/entidad", method = RequestMethod.POST)
	public String saveEntidad(Entidad entidad) {
		entidadService.saveEntidad(entidad);
		return "redirect:/entidades";
	}

	@RequestMapping(value="/entidad/{id}", method = RequestMethod.GET)
	public String viewEntidad(@PathVariable Integer id, Model model) {
		model.addAttribute("entidad", entidadService.getEntidadById(id));
		return "viewEntidad";
	}

	@RequestMapping(value="/entidad/edit/{id}", method = RequestMethod.GET)
	public String editEntidad(@PathVariable Integer id, Model model) {
		model.addAttribute("entidad", entidadService.getEntidadById(id));
		return "editEntidad";
	}

	@RequestMapping("/entidad/delete/{id}")
	public String delete(@PathVariable Integer id) {
		entidadService.deleteEntidad(id);
		return "redirect:/entidades";
	}
}
