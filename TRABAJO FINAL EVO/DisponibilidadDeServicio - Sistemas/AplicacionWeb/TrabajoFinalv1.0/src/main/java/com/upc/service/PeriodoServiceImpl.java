package com.upc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.upc.entity.Periodo;
import com.upc.repository.PeriodoRepository;

@Service
public class PeriodoServiceImpl implements PeriodoService{
	
	@Autowired
	private PeriodoRepository periodoRepository;

	@Override
	public Iterable<Periodo> listAllPeriodo() {
		// TODO Auto-generated method stub
		return periodoRepository.findAll();
	}

	@Override
	public Periodo getPeriodoById(Integer id) {
		// TODO Auto-generated method stub
		return periodoRepository.findOne(id);
	}

	@Override
	public Periodo savePeriodo(Periodo periodo) {
		// TODO Auto-generated method stub
		return periodoRepository.save(periodo);
	}

	@Override
	public void deletePerido(Integer id) {
		// TODO Auto-generated method stub
		periodoRepository.delete(id);
	}
	
}
