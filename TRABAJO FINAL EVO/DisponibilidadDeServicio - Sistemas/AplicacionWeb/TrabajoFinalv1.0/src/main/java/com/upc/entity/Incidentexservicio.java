package com.upc.entity;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class Incidentexservicio {

	private String startDate;
	private String endDate;
	private int minutos;
	private float horas;
	private float unavailability;
	private float availability;
	private String whomImpacted;
	private String impactDescription;
	private int calificacion;
	
	@ManyToOne
	@JoinColumn(name="id_incidente")
	private Incidente incidente;
	
	@ManyToOne
	@JoinColumn(name="id_servicio")
	private Servicio servicio;
	
	@ManyToOne
	@JoinColumn(name="color")
	private Impact_color impact_color;
	
	public Incidentexservicio() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public int getMinutos() {
		return minutos;
	}
	public void setMinutos(int minutos) {
		this.minutos = minutos;
	}
	public float getHoras() {
		return horas;
	}
	public void setHoras(float horas) {
		this.horas = horas;
	}
	public float getUnavailability() {
		return unavailability;
	}
	public void setUnavailability(float unavailability) {
		this.unavailability = unavailability;
	}
	public float getAvailability() {
		return availability;
	}
	public void setAvailability(float availability) {
		this.availability = availability;
	}
	public String getWhomImpacted() {
		return whomImpacted;
	}
	public void setWhomImpacted(String whomImpacted) {
		this.whomImpacted = whomImpacted;
	}
	public String getImpactDescription() {
		return impactDescription;
	}
	public void setImpactDescription(String impactDescription) {
		this.impactDescription = impactDescription;
	}
	public int getCalificacion() {
		return calificacion;
	}
	public void setCalificacion(int calificacion) {
		this.calificacion = calificacion;
	}
	public Incidente getIncidente() {
		return incidente;
	}
	public void setIncidente(Incidente incidente) {
		this.incidente = incidente;
	}
	public Servicio getServicio() {
		return servicio;
	}
	public void setServicio(Servicio servicio) {
		this.servicio = servicio;
	}
	public Impact_color getImpact_color() {
		return impact_color;
	}
	public void setImpact_color(Impact_color impact_color) {
		this.impact_color = impact_color;
	}
	
	
}
