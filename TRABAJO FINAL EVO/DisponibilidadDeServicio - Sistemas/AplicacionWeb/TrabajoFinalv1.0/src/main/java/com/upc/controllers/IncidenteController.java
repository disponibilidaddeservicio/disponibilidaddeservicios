package com.upc.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.upc.entity.Incidente;
import com.upc.service.IncidenteService;

@Controller
public class IncidenteController {
	
	@Autowired
	private IncidenteService incidenteservice;
	
	@RequestMapping(value= "/incidentes", method = RequestMethod.GET)
	public String list(Model model) {
		model.addAttribute("incidentes", incidenteservice.listAllIncidente());
		return "incidentes";
	}
	
	@RequestMapping("incidente/new")
	public String newIncidente(Model model){
		model.addAttribute("incidente", new Incidente());
		return "newIncidente";
	}
	
	@RequestMapping(value="/incidente", method = RequestMethod.POST)
	public String saveIncidente(Incidente incidente){
		incidenteservice.saveIncidente(incidente);
		return "redirect:/incidentes";
	}
	
	@RequestMapping("incidente/{id}")
	public String viewIncidente(@PathVariable Integer id, Model model){
		model.addAttribute("incidente", incidenteservice.getIncidenteById(id));
		return "viewIncidente";
	}
	
	@RequestMapping("incidente/edit/{id}")
	public String editIncidente(@PathVariable Integer id, Model model){
		model.addAttribute("incidente", incidenteservice.getIncidenteById(id));
		return "editIncidente";
	}
	
	@RequestMapping("incidente/delete/{id}")
	public String delete(@PathVariable Integer id){
		incidenteservice.deleteIncidente(id);
		return "viewIncidente";
	}

}
