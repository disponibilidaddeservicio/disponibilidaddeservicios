package com.upc.entity;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Servicio {
	private int id_servicio;
	private String nombre;
	private String descripcion;
	private Tipo_servicio tipo_servicio;
	
	public Servicio() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId_servicio() {
		return id_servicio;
	}

	public void setId_servicio(int id_servicio) {
		this.id_servicio = id_servicio;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	@ManyToOne
	@JoinColumn(name = "tipo")
	public Tipo_servicio getTipo_servicio() {
		return tipo_servicio;
	}

	public void setTipo_servicio(Tipo_servicio tipo_servicio) {
		this.tipo_servicio = tipo_servicio;
	}
}
