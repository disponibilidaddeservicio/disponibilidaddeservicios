package com.upc.service;

import com.upc.entity.Entidad;

public interface EntidadService {
	Iterable<Entidad> listAllEntidad();
	Entidad getEntidadById(Integer id);
	Entidad saveEntidad(Entidad entidad);
	void deleteEntidad(Integer id);
	Iterable<Entidad> getEntidadByPais(String pais);
}
