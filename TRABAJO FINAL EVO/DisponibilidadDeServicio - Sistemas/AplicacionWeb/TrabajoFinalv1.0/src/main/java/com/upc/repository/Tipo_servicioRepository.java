package com.upc.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.upc.entity.Tipo_servicio;

@Repository
@Transactional
public interface Tipo_servicioRepository extends CrudRepository<Tipo_servicio, Integer>{

}
