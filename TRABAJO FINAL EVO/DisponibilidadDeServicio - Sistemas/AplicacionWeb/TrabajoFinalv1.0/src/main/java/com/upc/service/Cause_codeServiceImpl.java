package com.upc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.upc.entity.Cause_code;
import com.upc.repository.Cause_codeRepository;

@Service
public class Cause_codeServiceImpl implements Cause_codeService{

	@Autowired
	private Cause_codeRepository cause_codeRepository;
	
	@Override
	public Iterable<Cause_code> listAllCauseCode() {
		// TODO Auto-generated method stub
		return cause_codeRepository.findAll();
	}

	@Override
	public Cause_code getCauseCodeById(Integer id) {
		// TODO Auto-generated method stub
		return cause_codeRepository.findOne(id);
	}

	@Override
	public Cause_code saveCauseCode(Cause_code cause_code) {
		// TODO Auto-generated method stub
		return cause_codeRepository.save(cause_code);
	}

	@Override
	public void deleteCauseCode(Integer id) {
		// TODO Auto-generated method stub
		cause_codeRepository.delete(id);
	}

}
