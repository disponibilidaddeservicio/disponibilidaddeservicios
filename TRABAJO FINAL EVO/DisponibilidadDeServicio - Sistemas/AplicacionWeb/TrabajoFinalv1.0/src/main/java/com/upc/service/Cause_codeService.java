package com.upc.service;

import com.upc.entity.Cause_code;

public interface Cause_codeService {

	Iterable<Cause_code> listAllCauseCode();
	Cause_code getCauseCodeById(Integer id);
	Cause_code saveCauseCode(Cause_code cause_code);
	void deleteCauseCode(Integer id);
}
