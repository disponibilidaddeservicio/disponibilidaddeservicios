package com.upc.entity;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="entidad")
public class Entidad {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id_entidad;
	
	private String nombre;
	private String pais; 
	private String descripcion;

	@OneToMany(mappedBy = "entidad")
	private Set<Incidente> incidentes;

	@OneToMany(mappedBy = "entidad")
	private Set<Usuario> usuarios;
	
	public Entidad() {
		super();
	}
	public int getId_entidad() {
		return id_entidad;
	}
	public void setId_entidad(int id_entidad) {
		this.id_entidad = id_entidad;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public Set<Incidente> getIncidentes() {
		return incidentes;
	}
	public void setIncidentes(Set<Incidente> incidentes) {
		this.incidentes = incidentes;
	}
	public Set<Usuario> getUsuarios() {
		return usuarios;
	}
	public void setUsuarios(Set<Usuario> usuarios) {
		this.usuarios = usuarios;
	}	
	
}
