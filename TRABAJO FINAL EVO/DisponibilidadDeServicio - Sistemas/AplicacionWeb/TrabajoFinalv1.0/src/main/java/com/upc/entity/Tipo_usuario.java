package com.upc.entity;

import javax.persistence.Entity;

import java.util.Set;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Tipo_usuario {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idtipo_usuario;
	private String nombre;
	private String descripcion;
	
	@OneToMany(mappedBy = "tipo_usuario")
	private Set<Usuario> usuarios;
		
	public Tipo_usuario() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public int getIdtipo_usuario() {
		return idtipo_usuario;
	}
	public void setIdtipo_usuario(int idtipo_usuario) {
		this.idtipo_usuario = idtipo_usuario;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Set<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(Set<Usuario> usuarios) {
		this.usuarios = usuarios;
	}
}
