package com.upc.entity;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Incidente {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id_incidente;
	private String rootCause;
	private int failedChange;
	private int rootUnknown;
	private String solution;
	private String prevention;
	private float duracion_hrs;

	@ManyToOne
	@JoinColumn(name = "entidad")
	private Entidad entidad;

	@ManyToOne
	@JoinColumn(name = "impactType")
	private Impact_type impacttype;

	@ManyToOne
	@JoinColumn(name = "causeCode")
	private Cause_code cause_code;

	@ManyToOne
	@JoinColumn(name = "periodo")
	private Periodo periodo;

	@OneToMany(mappedBy = "incidente")
	private Set<Derivacion> derivaciones;

	// @OneToMany(mappedBy = "incidente")
	// private Set<Incidentexservicio> incidentexservicio;

	public Incidente() {
		super();
	}

	public int getId_incidente() {
		return id_incidente;
	}

	public void setId_incidente(int id_incidente) {
		this.id_incidente = id_incidente;
	}

	public String getRootCause() {
		return rootCause;
	}

	public void setRootCause(String rootCause) {
		this.rootCause = rootCause;
	}

	public int getFailedChange() {
		return failedChange;
	}

	public void setFailedChange(int failedChange) {
		this.failedChange = failedChange;
	}

	public int getRootUnknown() {
		return rootUnknown;
	}

	public void setRootUnknown(int rootUnknown) {
		this.rootUnknown = rootUnknown;
	}

	public String getSolution() {
		return solution;
	}

	public void setSolution(String solution) {
		this.solution = solution;
	}

	public String getPrevention() {
		return prevention;
	}

	public void setPrevention(String prevention) {
		this.prevention = prevention;
	}

	public float getDuracion_hrs() {
		return duracion_hrs;
	}

	public void setDuracion_hrs(float duracion_hrs) {
		this.duracion_hrs = duracion_hrs;
	}

	// ForeignKey

	public Impact_type getImpacttype() {
		return impacttype;
	}

	public void setImpacttype(Impact_type impacttype) {
		this.impacttype = impacttype;
	}

	public Entidad getEntidad() {
		return entidad;
	}

	public void setEntidad(Entidad entidad) {
		this.entidad = entidad;
	}

	public Cause_code getCause_code() {
		return cause_code;
	}

	public void setCause_code(Cause_code cause_code) {
		this.cause_code = cause_code;
	}

	public Periodo getPeriodo() {
		return periodo;
	}

	public void setPeriodo(Periodo periodo) {
		this.periodo = periodo;
	}

	public Set<Derivacion> getDerivaciones() {
		return derivaciones;
	}

	public void setDerivaciones(Set<Derivacion> derivaciones) {
		this.derivaciones = derivaciones;
	}

	// public Set<Incidentexservicio> getIncidentexservicio() {
	// return incidentexservicio;
	// }
	//
	// public void setIncidentexservicio(Set<Incidentexservicio>
	// incidentexservicio) {
	// this.incidentexservicio = incidentexservicio;
	// }
}
