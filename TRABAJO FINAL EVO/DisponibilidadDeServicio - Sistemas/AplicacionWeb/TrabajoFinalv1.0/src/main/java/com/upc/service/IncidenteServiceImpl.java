package com.upc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.upc.entity.Incidente;
import com.upc.repository.IncidenteRepository;

@Service
public class IncidenteServiceImpl implements IncidenteService{

	@Autowired
	private IncidenteRepository incidenteRepository;
	
	@Override
	public Iterable<Incidente> listAllIncidente() {
		// TODO Auto-generated method stub
		return incidenteRepository.findAll();
	}

	@Override
	public Incidente getIncidenteById(Integer id) {
		// TODO Auto-generated method stub
		return incidenteRepository.findOne(id);
	}

	@Override
	public Incidente saveIncidente(Incidente incidente) {
		// TODO Auto-generated method stub
		return incidenteRepository.save(incidente);
	}

	@Override
	public void deleteIncidente(Integer id) {
		// TODO Auto-generated method stub
		incidenteRepository.delete(id);
	}

}
