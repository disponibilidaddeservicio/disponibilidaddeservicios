-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: serviceavailability_upc
-- ------------------------------------------------------
-- Server version	5.7.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cause_code`
--

DROP TABLE IF EXISTS `cause_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cause_code` (
  `id_cause_code` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(12) NOT NULL,
  `descripcion` varchar(440) DEFAULT NULL,
  PRIMARY KEY (`id_cause_code`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cause_code`
--

LOCK TABLES `cause_code` WRITE;
/*!40000 ALTER TABLE `cause_code` DISABLE KEYS */;
INSERT INTO `cause_code` VALUES (1,'Software','Falla de Sistema Operativo, Base de datos, etc.'),(2,'Hardware','Falla de hardware.'),(3,'Connectivity','Incidentes donde un outage/degrade provoca una perdida de connectividad dentro de la infraestructura del banco (incluyendo DNS, DHCP, Load Balancers).'),(4,'Applications','Falla de applicación (app propiedad de IT&S).'),(5,'Security','Incidentes donde un outage/degrade fue intencionalmente provocado para proteger  BNS de una amenaza externa (phishing, e-mail, etc).'),(6,'Process','Falla en el proceso.'),(7,'Third Party','Incidentes causados por un servicio proviniente de un proveedor externo (i.e. Bell, Reuters, Symcor, etc).'),(8,'Utilities','Incidentes donde un outage/degrade fueron intencionalmente provocados (decisión administrativa) o se dieron por razones fuera del control de IT&S (clima, electricidad, etc).');
/*!40000 ALTER TABLE `cause_code` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `derivacion`
--

DROP TABLE IF EXISTS `derivacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `derivacion` (
  `idderivacion` int(11) NOT NULL AUTO_INCREMENT,
  `incidente` int(11) NOT NULL,
  `area` varchar(20) NOT NULL,
  `inicio` datetime NOT NULL,
  `fin` datetime NOT NULL,
  `descripcion` varchar(440) DEFAULT NULL,
  PRIMARY KEY (`idderivacion`),
  KEY `FK_DERIVACION_INCIDENTE_idx` (`incidente`),
  CONSTRAINT `FK_DERIVACION_INCIDENTE` FOREIGN KEY (`incidente`) REFERENCES `incidente` (`id_incidente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FKi9oqo31tg4yh226fyydky15mw` FOREIGN KEY (`incidente`) REFERENCES `incidente` (`id_incidente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `derivacion`
--

LOCK TABLES `derivacion` WRITE;
/*!40000 ALTER TABLE `derivacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `derivacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entidad`
--

DROP TABLE IF EXISTS `entidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entidad` (
  `id_entidad` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(11) NOT NULL,
  `pais` varchar(20) NOT NULL,
  `descripcion` varchar(140) DEFAULT NULL,
  PRIMARY KEY (`id_entidad`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entidad`
--

LOCK TABLES `entidad` WRITE;
/*!40000 ALTER TABLE `entidad` DISABLE KEYS */;
INSERT INTO `entidad` VALUES (1,'UPC','PERU',''),(2,'CSF','PERU',NULL),(3,'SBC','CHILE',NULL),(4,'COLPATRIA','COLOMBIA',NULL),(5,'COLFONDOS','COLOMBIA',NULL),(6,'SBSLV','EL SALVADOR',NULL),(7,'SBPA','PANAMA',NULL),(8,'SBCRI','COSTA RICA',NULL),(9,'MC-LATAM','PERU','Centro Regional - Gestión de Procesos');
/*!40000 ALTER TABLE `entidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entidadxservicio`
--

DROP TABLE IF EXISTS `entidadxservicio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entidadxservicio` (
  `id_entidad` int(11) NOT NULL,
  `id_servicio` int(11) NOT NULL,
  `horario` varchar(50) NOT NULL,
  `mantenimiento` varchar(50) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `cant_clientes` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_entidad`,`id_servicio`),
  KEY `FK_ENTIDADxSERVICIO_SERVICIO` (`id_servicio`),
  CONSTRAINT `FK_ENTIDADxSERVICIO_ENTIDAD` FOREIGN KEY (`id_entidad`) REFERENCES `entidad` (`id_entidad`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_ENTIDADxSERVICIO_SERVICIO` FOREIGN KEY (`id_servicio`) REFERENCES `servicio` (`id_servicio`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entidadxservicio`
--

LOCK TABLES `entidadxservicio` WRITE;
/*!40000 ALTER TABLE `entidadxservicio` DISABLE KEYS */;
INSERT INTO `entidadxservicio` VALUES (1,1,'24x7',NULL,NULL,NULL),(1,2,'24x7',NULL,NULL,NULL),(1,3,'24x7',NULL,NULL,NULL),(1,4,'24x7',NULL,NULL,NULL),(1,5,'24x7',NULL,NULL,NULL),(1,6,'24x7',NULL,NULL,NULL),(1,7,'24x7',NULL,NULL,NULL),(1,8,'8am a 9pm',NULL,NULL,NULL),(1,9,'8am a 9pm',NULL,NULL,NULL),(1,10,'L-V 8hrs',NULL,NULL,NULL),(1,11,'24x7',NULL,NULL,NULL),(2,1,'24x7',NULL,NULL,NULL),(2,2,'24x7',NULL,NULL,NULL),(2,4,'24x7',NULL,NULL,NULL),(2,5,'24x7',NULL,NULL,NULL),(2,6,'24x7',NULL,NULL,NULL),(2,11,'24x7',NULL,NULL,NULL),(3,1,'24x7',NULL,NULL,NULL),(3,2,'24x7',NULL,NULL,NULL),(3,3,'24x7',NULL,NULL,NULL),(3,4,'24x7',NULL,NULL,NULL),(3,5,'24x7',NULL,NULL,NULL),(3,6,'24x7',NULL,NULL,NULL),(3,7,'24x7',NULL,NULL,NULL),(3,10,'L-V 8hrs',NULL,NULL,NULL),(3,11,'24x7',NULL,NULL,NULL),(4,1,'24x7',NULL,NULL,NULL),(4,2,'24x7',NULL,NULL,NULL),(4,3,'24x7',NULL,NULL,NULL),(4,4,'24x7',NULL,NULL,NULL),(4,5,'24x7',NULL,NULL,NULL),(4,6,'24x7',NULL,NULL,NULL),(4,7,'24x7',NULL,NULL,NULL),(4,10,'L-V 8hrs',NULL,NULL,NULL),(4,11,'24x7',NULL,NULL,NULL),(5,4,'24x7',NULL,NULL,NULL),(5,5,'24x7',NULL,NULL,NULL),(5,6,'24x7',NULL,NULL,NULL),(6,1,'24x7',NULL,NULL,NULL),(6,2,'24x7',NULL,NULL,NULL),(6,3,'24x7',NULL,NULL,NULL),(6,4,'24x7',NULL,NULL,NULL),(6,5,'24x7',NULL,NULL,NULL),(6,6,'24x7',NULL,NULL,NULL),(6,7,'24x7',NULL,NULL,NULL),(6,11,'24x7',NULL,NULL,NULL),(7,1,'24x7',NULL,NULL,NULL),(7,2,'24x7',NULL,NULL,NULL),(7,3,'24x7',NULL,NULL,NULL),(7,4,'24x7',NULL,NULL,NULL),(7,5,'24x7',NULL,NULL,NULL),(7,6,'24x7',NULL,NULL,NULL),(7,7,'24x7',NULL,NULL,NULL),(7,11,'24x7',NULL,NULL,NULL),(8,1,'24x7',NULL,NULL,NULL),(8,2,'24x7',NULL,NULL,NULL),(8,3,'24x7',NULL,NULL,NULL),(8,4,'24x7',NULL,NULL,NULL),(8,5,'24x7',NULL,NULL,NULL),(8,6,'24x7',NULL,NULL,NULL),(8,7,'24x7',NULL,NULL,NULL),(8,11,'24x7',NULL,NULL,NULL);
/*!40000 ALTER TABLE `entidadxservicio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `impact_color`
--

DROP TABLE IF EXISTS `impact_color`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `impact_color` (
  `id_impact_color` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(7) NOT NULL,
  `descripcion` varchar(140) DEFAULT NULL,
  PRIMARY KEY (`id_impact_color`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `impact_color`
--

LOCK TABLES `impact_color` WRITE;
/*!40000 ALTER TABLE `impact_color` DISABLE KEYS */;
INSERT INTO `impact_color` VALUES (1,'Minimal','impacto minimo'),(2,'Minor','impacto medio'),(3,'Major','impacto alto');
/*!40000 ALTER TABLE `impact_color` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `impact_type`
--

DROP TABLE IF EXISTS `impact_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `impact_type` (
  `id_impact_type` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(7) NOT NULL,
  `descripcion` varchar(140) DEFAULT NULL,
  PRIMARY KEY (`id_impact_type`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `impact_type`
--

LOCK TABLES `impact_type` WRITE;
/*!40000 ALTER TABLE `impact_type` DISABLE KEYS */;
INSERT INTO `impact_type` VALUES (1,'Degrade','incluye partial loss'),(2,'Outage','corte total');
/*!40000 ALTER TABLE `impact_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `incidente`
--

DROP TABLE IF EXISTS `incidente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `incidente` (
  `id_incidente` int(11) NOT NULL AUTO_INCREMENT,
  `entidad` int(11) NOT NULL,
  `rootCause` varchar(440) NOT NULL,
  `failedChange` smallint(6) NOT NULL,
  `rootUnknown` smallint(6) NOT NULL,
  `solution` varchar(440) NOT NULL,
  `prevention` varchar(440) NOT NULL,
  `impactType` int(11) NOT NULL,
  `causeCode` int(11) NOT NULL,
  `periodo` int(11) NOT NULL,
  `duracion_hrs` float NOT NULL,
  `failed_change` int(11) NOT NULL,
  `root_cause` varchar(255) DEFAULT NULL,
  `root_unknown` int(11) NOT NULL,
  `cause_code` int(11) DEFAULT NULL,
  `impact_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_incidente`),
  KEY `FK_INCIDENTE_PERIODO` (`periodo`),
  KEY `FK_INCIDENTE_ENTIDAD_idx` (`entidad`),
  KEY `FKrff1gb06bs2tmq1njvtbj5wq4` (`cause_code`),
  KEY `FKm2pteyb3nnv8ueec6nlt5umt2` (`impact_type`),
  CONSTRAINT `FK5yq0fcj85nfoon1otfwem33le` FOREIGN KEY (`entidad`) REFERENCES `entidad` (`id_entidad`),
  CONSTRAINT `FK_INCIDENTE_ENTIDAD` FOREIGN KEY (`entidad`) REFERENCES `entidad` (`id_entidad`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_INCIDENTE_PERIODO` FOREIGN KEY (`periodo`) REFERENCES `periodo` (`id_periodo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FKm2pteyb3nnv8ueec6nlt5umt2` FOREIGN KEY (`impact_type`) REFERENCES `impact_type` (`id_impact_type`),
  CONSTRAINT `FKqnbl294hbetmo3rtqcjwgye7u` FOREIGN KEY (`periodo`) REFERENCES `periodo` (`id_periodo`),
  CONSTRAINT `FKrff1gb06bs2tmq1njvtbj5wq4` FOREIGN KEY (`cause_code`) REFERENCES `cause_code` (`id_cause_code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `incidente`
--

LOCK TABLES `incidente` WRITE;
/*!40000 ALTER TABLE `incidente` DISABLE KEYS */;
INSERT INTO `incidente` VALUES (1,1,'Autorizadores en AS400 bloqueados',0,0,'Se cancelaron los trabajos de TOLD con mensaje de bloqueo para liberarlos.','Relevar los tipos de mensajes de error para un siguiente evento recurrente',2,3,13,0.38,0,NULL,0,1,1),(2,3,'Falla eléctrica afecta al  Switch de distribución del edificio Morande 226.',0,0,'Se escala al área de redes quienes detectan que la UPS del rack de comunicaciones  del piso 4 se encuentra desconectada, debido a trabajos de mantenimiento físico.','Re-ubicación del Switch de distribución a un sitio con condiciones seguras.',2,2,4,0.2,0,NULL,0,1,1);
/*!40000 ALTER TABLE `incidente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `incidentexservicio`
--

DROP TABLE IF EXISTS `incidentexservicio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `incidentexservicio` (
  `id_incidente` int(11) NOT NULL,
  `id_servicio` int(11) NOT NULL,
  `startDate` datetime NOT NULL,
  `endDate` datetime NOT NULL,
  `minutos` int(11) NOT NULL,
  `horas` float NOT NULL,
  `unavailability` float NOT NULL,
  `availability` float NOT NULL,
  `whomImpacted` varchar(8) NOT NULL,
  `impactDescription` varchar(440) NOT NULL,
  `calificacion` int(11) NOT NULL,
  `color` int(11) NOT NULL,
  PRIMARY KEY (`id_incidente`,`id_servicio`),
  KEY `FK_INCIDENTExSERVICIO_IMPACT_COLOR` (`color`),
  KEY `FK_INCIDENTExSERVICIO_SERVICIO` (`id_servicio`),
  CONSTRAINT `FK_INCIDENTExSERVICIO_IMPACT_COLOR` FOREIGN KEY (`color`) REFERENCES `impact_color` (`id_impact_color`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_INCIDENTExSERVICIO_INCIDENTE` FOREIGN KEY (`id_incidente`) REFERENCES `incidente` (`id_incidente`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_INCIDENTExSERVICIO_SERVICIO` FOREIGN KEY (`id_servicio`) REFERENCES `servicio` (`id_servicio`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `incidentexservicio`
--

LOCK TABLES `incidentexservicio` WRITE;
/*!40000 ALTER TABLE `incidentexservicio` DISABLE KEYS */;
INSERT INTO `incidentexservicio` VALUES (1,1,'2016-09-02 17:11:00','2016-09-02 17:34:00',23,0.38,0.056,99.944,'Customer','Rechazo total transacciones',64,2),(1,2,'2016-09-02 17:22:00','2016-09-02 17:34:00',12,0.2,0.032,99.968,'Customer','Rechazo total transacciones',64,2),(2,6,'2015-12-10 14:28:00','2015-12-10 14:40:00',12,0.2,0.1,99.9,'Staff','Pérdida de conectividad en los Pcs y teléfonos IP',30,1);
/*!40000 ALTER TABLE `incidentexservicio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `periodo`
--

DROP TABLE IF EXISTS `periodo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `periodo` (
  `id_periodo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(14) NOT NULL,
  `semanas` smallint(6) NOT NULL,
  `fecha_ini` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `quarter` int(11) NOT NULL,
  `fiscal_year` int(11) NOT NULL,
  `request_date` date NOT NULL,
  `delivery_date` date NOT NULL,
  PRIMARY KEY (`id_periodo`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `periodo`
--

LOCK TABLES `periodo` WRITE;
/*!40000 ALTER TABLE `periodo` DISABLE KEYS */;
INSERT INTO `periodo` VALUES (1,'Setiembre_FY15',4,'2015-08-15','2015-09-11',4,2015,'2015-09-15','2015-09-23'),(2,'Octubre_FY15',4,'2015-09-12','2015-10-09',4,2015,'2015-10-13','2015-10-21'),(3,'Noviembre_FY16',5,'2015-10-10','2015-11-13',1,2016,'2015-11-17','2015-11-25'),(4,'Diciembre_FY16',4,'2015-11-14','2015-12-11',1,2016,'2015-12-15','2015-12-23'),(5,'Enero_FY16',4,'2015-12-12','2016-01-08',1,2016,'2016-01-12','2016-01-20'),(6,'Febrero_FY16',5,'2016-01-09','2016-02-12',2,2016,'2016-02-16','2016-02-24'),(7,'Marzo_FY16',4,'2016-02-13','2016-03-11',2,2016,'2016-03-15','2016-03-23'),(8,'Abril_FY16',4,'2016-03-12','2016-04-08',2,2016,'2016-04-12','2016-04-20'),(9,'Mayo_FY16',5,'2016-04-09','2016-05-13',3,2016,'2016-05-17','2016-05-25'),(10,'Junio_FY16',4,'2016-05-14','2016-06-10',3,2016,'2016-06-14','2016-06-22'),(11,'Julio_FY16',4,'2016-06-11','2016-07-08',3,2016,'2016-07-12','2016-07-20'),(12,'Agosto_FY16',5,'2016-07-09','2016-08-12',4,2016,'2016-08-16','2016-08-24'),(13,'Setiembre_FY16',4,'2016-08-13','2016-09-09',4,2016,'2016-09-13','2016-09-21'),(14,'Octubre_FY16',4,'2016-09-10','2016-10-07',4,2016,'2016-10-11','2016-10-19');
/*!40000 ALTER TABLE `periodo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servicio`
--

DROP TABLE IF EXISTS `servicio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servicio` (
  `id_servicio` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) NOT NULL,
  `descripcion` varchar(440) DEFAULT NULL,
  `tipo` int(11) NOT NULL,
  PRIMARY KEY (`id_servicio`),
  KEY `FK_SERVICIO_TIPO_SERVICIO_idx` (`tipo`),
  CONSTRAINT `FK_SERVICIO_TIPO_SERVICIO` FOREIGN KEY (`tipo`) REFERENCES `tipo_servicio` (`id_tipo_servicio`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FKl6auybbo7hpbgldrncqa0y90n` FOREIGN KEY (`tipo`) REFERENCES `tipo_servicio` (`id_tipo_servicio`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicio`
--

LOCK TABLES `servicio` WRITE;
/*!40000 ALTER TABLE `servicio` DISABLE KEYS */;
INSERT INTO `servicio` VALUES (1,'ATM',NULL,1),(2,'POS',NULL,1),(3,'ONLINE BANKING',NULL,1),(4,'CALL CENTER',NULL,1),(5,'INTRANET',NULL,2),(6,'NETWORK',NULL,3),(7,'AS400',NULL,3),(8,'SEDAPAL',NULL,4),(9,'EDELNOR',NULL,4),(10,'LEASING',NULL,2),(11,'VISA',NULL,4);
/*!40000 ALTER TABLE `servicio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_servicio`
--

DROP TABLE IF EXISTS `tipo_servicio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_servicio` (
  `id_tipo_servicio` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) NOT NULL,
  `descripcion` varchar(140) DEFAULT NULL,
  PRIMARY KEY (`id_tipo_servicio`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_servicio`
--

LOCK TABLES `tipo_servicio` WRITE;
/*!40000 ALTER TABLE `tipo_servicio` DISABLE KEYS */;
INSERT INTO `tipo_servicio` VALUES (1,'Delivery Channel','canal de servicio'),(2,'Applications',NULL),(3,'Infraestructure',NULL),(4,'Third Party Services','proveedores');
/*!40000 ALTER TABLE `tipo_servicio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_usuario`
--

DROP TABLE IF EXISTS `tipo_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_usuario` (
  `idtipo_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) NOT NULL,
  `descripcion` varchar(140) NOT NULL,
  PRIMARY KEY (`idtipo_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_usuario`
--

LOCK TABLES `tipo_usuario` WRITE;
/*!40000 ALTER TABLE `tipo_usuario` DISABLE KEYS */;
INSERT INTO `tipo_usuario` VALUES (1,'admin','todos los permisos'),(2,'vip','solo vizualiza'),(3,'especialista','registra, modifica y vizualiza');
/*!40000 ALTER TABLE `tipo_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `correo` varchar(45) NOT NULL,
  `nick` varchar(10) NOT NULL,
  `password` varchar(15) NOT NULL,
  `tipo` int(11) NOT NULL,
  `entidad` int(11) NOT NULL,
  PRIMARY KEY (`idusuario`),
  KEY `FK_USUARIO_TIPO_idx` (`tipo`),
  KEY `FK_USUARIO_ENTIDAD_idx` (`entidad`),
  CONSTRAINT `FK95niqvg4s5vx5wh5r2f2dp5ck` FOREIGN KEY (`entidad`) REFERENCES `entidad` (`id_entidad`),
  CONSTRAINT `FK_USUARIO_ENTIDAD` FOREIGN KEY (`entidad`) REFERENCES `entidad` (`id_entidad`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_USUARIO_TIPO_USUARIO` FOREIGN KEY (`tipo`) REFERENCES `tipo_usuario` (`idtipo_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FKqivnnrr4hgdni3sx7ur61fa7y` FOREIGN KEY (`tipo`) REFERENCES `tipo_usuario` (`idtipo_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'administrador@ibm.com','charmander','123',1,9),(2,'ti_local_chl@ibm.com','squirtle','456',3,3),(3,'svp_chl@ibm.com','pikachu','789',2,3),(4,'ti_local_col@ibm.com','bulbasaur','321',1,4),(5,'svp_col@ibm.com','pidget','654',2,4);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-24 22:16:39
